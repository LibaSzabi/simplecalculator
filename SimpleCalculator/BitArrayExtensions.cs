﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleCalculator
{
    public static class BitArrayExtensions
    {
        public static int NumberSizeInStack { get; set; }

        public static BitArray To10BitArray(this string value)
        {
            var returnAsException = false;
            if (!Int32.TryParse(value, out int int32Result))
            {
                returnAsException = true;
            }
          
            var numberIn32Bit = new bool[32];
            var numberIn10Bit = new bool[10];

            var bitArray = new BitArray(new[] { int32Result });
            bitArray.CopyTo(numberIn32Bit, 0);

            for (int i = 0; i < numberIn32Bit.Length; i++)
            {
                if (i > NumberSizeInStack - 1)
                {
                    if (numberIn32Bit[i] == true)
                    {
                        returnAsException = true;
                        break;
                    }
                }
                else
                {
                    numberIn10Bit[i] = numberIn32Bit[i];
                }
            }
            var res = new BitArray(numberIn10Bit);

            if (returnAsException)
            {
                throw new StackOverflowException($"The stack can store the numbers in { NumberSizeInStack } bit ( {value} ) !");
            }
            return res;
        }
        public static int ToIntFromBitArray(this BitArray bitArray)
        {
            var array = new int[1];
            bitArray.CopyTo(array, 0);
            return array[0];
        }
    }
}
