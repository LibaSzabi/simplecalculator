﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleCalculator
{
    public interface ISimpleCalculator
    {
        /// <summary>
        /// Push <value>
        /// Pushes<value> on the memory stack.The stack has a maximum capacity of 5. When the stack
        /// capacity is exceeded the command results in error. <value> is guaranteed to be a valid
        /// </summary>
        /// <param name="value"></param>
        void Push(string value);

        /// <summary>
        /// Add
        /// Pops two values from the stack, adds them together, prints the result and pushes the result back
        /// to the stack. If the required operands are not on the stack the command results in error
        /// </summary>
        /// <returns>The </returns>
        int Add();

        /// <summary>
        /// Sub
        /// Pops two values from the stack, subtracts the second topmost from the topmost prints the result,
        /// and pushes the result back to the stack.If the required operands are not on the stack the
        /// command results in error
        /// </summary>
        /// <returns></returns>
        int Sub();

        /// <summary>
        /// Pop
        /// Pops a value from the memory stack and discards it.If no values on the stack no operation is
        /// Performed
        /// </summary>
        void Pop();
    }
}
