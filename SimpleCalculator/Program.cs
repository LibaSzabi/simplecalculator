﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleCalculator
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            Console.WriteLine("Please choose an option:");
            Console.WriteLine("1) Push <value>");
            Console.WriteLine("2) Pop");
            Console.WriteLine("3) Add");
            Console.WriteLine("4) Sub");
            Console.WriteLine("5) Exit\r\n\n");

            var simpleCalculator = new SimpleCalculator();
            while (true)
            {
                try
                {
                    switch (Console.ReadLine())
                    {
                        case "1":
                            {
                                Console.WriteLine("Please enter the number:");
                                var numberInString = Console.ReadLine();
                                simpleCalculator.Push(numberInString);
                                simpleCalculator.ToString();
                                break;
                            }
                        case "2":
                            {
                                simpleCalculator.Pop();
                                simpleCalculator.ToString();
                                break;
                            }
                        case "3":
                            {
                                simpleCalculator.Add();
                                simpleCalculator.ToString();
                                break;
                            }
                        case "4":
                            {
                                simpleCalculator.Sub();
                                simpleCalculator.ToString();
                                break;
                            }
                        case "5":
                            {
                                Environment.Exit(0);
                                break;
                            }
                        default:
                            break;
                    }
                }
                catch (StackOverflowException soe)
                {
                    Console.WriteLine(soe.Message);
                }
                catch (InvalidOperationException ioe)
                {
                    Console.WriteLine(ioe.Message);
                }
            }

        }
    }
}
