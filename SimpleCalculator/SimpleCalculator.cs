﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleCalculator
{
    /// <summary>
    /// You’re tasked with implementing a simple calculator. The calculator stores numbers as 10 bit
    /// unsigned integers on an internal stack and has the following commands
    /// 
    /// Rules:
    /// A computation involving unsigned operands can never overflow. A result that cannot be
    /// represented by the resulting unsigned integer type is reduced modulo the number that is one
    /// greater than the largest value that can be represented by the resulting type.
    /// Example 5-7= 1022 (because -2 mod 1024 = 1022)
    /// Example interaction with the calculator:
    /// Push 10 (stack is 10)
    /// Push 7 (stack is 10, 7)
    /// Add
    /// >> 17 (stack is 17)
    /// Push 25 (stack is 17, 25)
    /// Sub
    /// >> 8 (stack is 8)
    /// 
    /// </summary>
    public class SimpleCalculator : ISimpleCalculator
    {
        /// <summary>
        /// Min value is 1 max is 32
        /// </summary>
        private const short STOREDNUMBERSINSTACKSIZEINBIT = 10;

        /// <summary>
        /// Min value is 1 max is short max value
        /// </summary>
        private const short MAXIMUMSTACKSIZE = 5;

        /// <summary>
        /// Min value is 2 max is MAXIMUMSTACKSIZE value
        /// </summary>
        private const short MINIMUMSTACKSIZETOFUNCTIONS = 2;

        private Stack<BitArray> _numbersStack;

        public SimpleCalculator()
        {
            BitArrayExtensions.NumberSizeInStack = STOREDNUMBERSINSTACKSIZEINBIT;
            _numbersStack = new Stack<BitArray>();
        }

        /// <summary>
        /// Method that's validate the stack size.
        /// </summary>
        private void ValidateStack()
        {
            if (_numbersStack.Count >= MAXIMUMSTACKSIZE)
            {
                throw new StackOverflowException($"The stack has a maximum capacity of { MAXIMUMSTACKSIZE }. Please pop the numbers and try again.");
            }
        }

        /// <summary>
        /// Push <value>
        /// Pushes<value> on the memory stack.The stack has a maximum capacity of 5. When the stack
        /// capacity is exceeded the command results in error. <value> is guaranteed to be a valid
        /// </summary>
        /// <param name="value"></param>
        public void Push(string value)
        {
            Console.WriteLine($"\nPush {value}");
            ValidateStack();
            _numbersStack.Push(value.To10BitArray());
        }

        /// <summary>
        /// Add
        /// Pops two values from the stack, adds them together, prints the result and pushes the result back
        /// to the stack. If the required operands are not on the stack the command results in error
        /// </summary>
        /// <returns>The </returns>
        public int Add()
        {
            Console.WriteLine("\nAfter additional:");
            ValidateStack();
            if (_numbersStack.Count < MINIMUMSTACKSIZETOFUNCTIONS)
            {
                throw new InvalidOperationException($"You only have { _numbersStack.Count } number in the stack! Please push more than { _numbersStack.Count } number on it.");
            }
         
            var num1 = _numbersStack.Pop().ToIntFromBitArray();
            var num2 = _numbersStack.Pop().ToIntFromBitArray();

            var intResult = num1 + num2;

            _numbersStack.Push(intResult.ToString().To10BitArray());
            return intResult;
        }

        /// <summary>
        /// Sub
        /// Pops two values from the stack, subtracts the second topmost from the topmost prints the result,
        /// and pushes the result back to the stack.If the required operands are not on the stack the
        /// command results in error
        /// </summary>
        /// <returns></returns>
        public int Sub()
        {
            Console.WriteLine("\nAfter substaction:");
            ValidateStack();
            if (_numbersStack.Count < MINIMUMSTACKSIZETOFUNCTIONS)
            {
                throw new InvalidOperationException($"You only have { _numbersStack.Count } number in the stack! Please push more than { _numbersStack.Count } number on it.");
            }

            var num1 = _numbersStack.Pop().ToIntFromBitArray();
            var num2 = _numbersStack.Pop().ToIntFromBitArray();

            var intResult = num1 - num2;

            _numbersStack.Push(intResult.ToString().To10BitArray());

            return intResult;
        }

        /// <summary>
        /// Pop
        /// Pops a value from the memory stack and discards it.If no values on the stack no operation is
        /// Performed
        /// </summary>
        public void Pop()
        {
            _numbersStack.Pop();
            Console.WriteLine("\nAfter pop:");
        }

        /// <summary>
        /// The stack container elements for the view.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var builder = new StringBuilder();
            for (int i = 0; i < _numbersStack.Count; i++)
            {
                builder.Append(string.Concat(Convert.ToString(_numbersStack.ToList()[i].ToIntFromBitArray()), _numbersStack.Count - 1 == i ? string.Empty : ", "));
            }
            Console.WriteLine($"\n(stack is { (_numbersStack.Count > 0 ? builder.ToString() : "empty") })");

            return base.ToString();
        }
    }
}
